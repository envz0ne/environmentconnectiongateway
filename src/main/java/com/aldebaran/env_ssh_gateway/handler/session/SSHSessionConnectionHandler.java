package com.aldebaran.env_ssh_gateway.handler.session;

import static com.aldebaran.env_ssh_gateway.util.SshUtils.exec;

import com.aldebaran.env_ssh_gateway.handler.environment.ConnectionEnvironmentHandler;
import com.aldebaran.env_ssh_gateway.model.CmdDTO;
import com.aldebaran.env_ssh_gateway.model.ConnSessionStatus;
import com.aldebaran.env_ssh_gateway.model.ConnectionResponse;
import com.aldebaran.env_ssh_gateway.model.ConnectionSession;
import com.aldebaran.env_ssh_gateway.service.SessionConnectionHandler;
import java.time.Duration;
import java.time.ZonedDateTime;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.jboss.logging.Logger;

/**
 * Represents the ssh session handler.
 *
 * @author Tarasov Ivan
 * @see 0.0.1
 */
@ApplicationScoped
public class SSHSessionConnectionHandler implements SessionConnectionHandler {

  @Inject
  ConnectionEnvironmentHandler exceptionHandler;

  private static final Logger LOGGER = Logger.getLogger(SSHSessionConnectionHandler.class);

  @ConfigProperty(name = "ssh.connection.retry.limit")
  int retryLimit;

  @ConfigProperty(name = "ssh.connection.timelife")
  int timelife;

  /**
   * Execute a cmd over the ssh session.
   *
   * @param session that needs to be connected
   * @param cmd that needs to be executed over ssh connection
   * @return session response
   */
  @Override
  public ConnectionResponse execCmd(ConnectionSession session, CmdDTO cmd) {

    // ssh session connection response
    ConnectionResponse res = new ConnectionResponse();

    // Checking time interval overall
    if (Duration.between(session.getCreated(), ZonedDateTime.now()).abs().toHours() > timelife) {
      res.setMsg("Session timelife is out of timelife" +
          "Duration difference is: " +
          Math.abs(Duration.between(session.getCreated(), ZonedDateTime.now()).abs().toHours() - timelife));

      res.setStatus(ConnSessionStatus.NEED_TO_CLOSE);

      LOGGER.info("SSH connection to environment ( id: " + session.getEnvId() + "; url:  " + session.getUrl() + " ). "
          + " is out of timelife. Duration difference is: " +
          Math.abs(Duration.between(session.getCreated(), ZonedDateTime.now()).abs().toHours() - timelife));

      return res;
    }

    String userPsswd = session.getLogin() + ":" + session.getPassword();

    String connectUri =
        "ssh://" + userPsswd + "@" + session.getUrl().substring(6);

    String out = "";

    try {
      out = exec(
          connectUri,
          "docker-enter " +
              session.getHosts().get(0).getContainerId() +
              " " + cmd.getCmd() + "\n"
      );

      res.setStatus(ConnSessionStatus.OK);
    } catch (Exception exp) {
      LOGGER.error("Error appears while connection: " + exp.getMessage());

      out = exp.getMessage();
      res.setStatus(ConnSessionStatus.CONNECTION_EXCEPTION);
    }

    res.setMsg(out);

    return res;
  }
}