package com.aldebaran.env_ssh_gateway.handler.environment;

import com.aldebaran.env_ssh_gateway.rest_client.model.env.EnvironmentStatus;

public class EnvironmentRunningStatus {

  private boolean running;

  private EnvironmentStatus status;

  public boolean isRunning() {
    return running;
  }

  public void setRunning(boolean running) {
    this.running = running;
  }

  public EnvironmentStatus getStatus() {
    return status;
  }

  public void setStatus(EnvironmentStatus status) {
    this.status = status;
  }
}
