package com.aldebaran.env_ssh_gateway.handler.environment;

import com.aldebaran.env_ssh_gateway.model.ConnectionSession;
import com.aldebaran.env_ssh_gateway.rest_client.EnvironmentProviderService;
import com.aldebaran.env_ssh_gateway.rest_client.model.env.EnvironmentResponseDTO;
import com.aldebaran.env_ssh_gateway.rest_client.model.env.EnvironmentStatus;
import com.aldebaran.env_ssh_gateway.rest_client.model.host.HostStatus;
import com.aldebaran.env_ssh_gateway.rest_client.model.host.HostStatusDTO;
import com.aldebaran.env_ssh_gateway.rest_client.model.host.HostStatusInfo;
import java.util.UUID;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.core.Response;
import org.eclipse.microprofile.rest.client.inject.RestClient;

@ApplicationScoped
public class ConnectionEnvironmentHandler {

  @Inject
  @RestClient
  EnvironmentProviderService service;

  /**
   * Handle ssh exception. Environment can be restarted by it's id.
   *
   * @param session is the current ssh session to the environment
   * @return true when environment has been restarted and false when not
   */
  public boolean handleException(ConnectionSession session) {

    boolean isRunning = isEnvironmentRunning(session.getEnvId()).isRunning();

    if (!isRunning) {
      // Send to env provider request to up/unpause environment
      Response response = service.restartEnvironment(session.getEnvId());
      EnvironmentResponseDTO restartResponse = response.readEntity(EnvironmentResponseDTO.class);

      // Environment has been restarted
      // Can't restart environment

      isRunning = restartResponse.getInfo().getStatus() == EnvironmentStatus.RUNNING;
    }

    // All environment's hosts are running.
    return isRunning;
  }

  /**
   * Get environment running status.
   *
   * @param envId is the environment id
   * @return true when environment is running and false when not
   */
  public EnvironmentRunningStatus isEnvironmentRunning(UUID envId) {

    // Get environment's hosts status
    Response response = service.getEnvHostsStatus(envId);
    HostStatusDTO hostEnvStatus = response.readEntity(HostStatusDTO.class);

    EnvironmentRunningStatus status = new EnvironmentRunningStatus();
    status.setRunning(true);

    // Check if env has hosts at all and if it could be possible to get env hosts' status.
    // In case not, it will be reconsidering as the environment isn't running
    if (
        hostEnvStatus.getHosts() == null || hostEnvStatus.getHosts().size() == 0 ||
            hostEnvStatus.getEnvStatus().getStatus() != EnvironmentStatus.RUNNING
    ) {
      status.setRunning(false);
    } else {
      // Get general hosts status
      for (HostStatusInfo o : hostEnvStatus.getHosts()) {
        if (o.getStatus() != HostStatus.RUNNING) {
          status.setRunning(false);
          return status;
        }
      }
    }
    status.setStatus(hostEnvStatus.getEnvStatus().getStatus());

    System.out.println(hostEnvStatus.getEnvStatus().getStatus());

    return status;
  }
}