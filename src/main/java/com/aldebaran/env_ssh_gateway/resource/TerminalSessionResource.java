package com.aldebaran.env_ssh_gateway.resource;


import com.aldebaran.env_ssh_gateway.model.CmdDTO;
import com.aldebaran.env_ssh_gateway.model.ConnSessionStatus;
import com.aldebaran.env_ssh_gateway.model.ConnectionResponse;
import com.aldebaran.env_ssh_gateway.model.ConnectionSession;
import com.aldebaran.env_ssh_gateway.service.impl.SSHSessionService;
import java.util.List;
import java.util.UUID;
import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.jboss.logging.Logger;

@Path("sessions")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@RequestScoped
public class TerminalSessionResource {

  @Inject
  SSHSessionService service;

  private static final Logger LOGGER = Logger.getLogger(TerminalSessionResource.class);

  /**
   * Execute a cmd by envId.
   *
   * @param cmd is an object with envId and cmd
   * @return session response
   */
  @POST
  //@Timeout(5000)
  //@Fallback(fallbackMethod = "fallbackTimeoutRecommendations")
  //@RolesAllowed({"user", "admin"})
  public Response doCmd(CmdDTO cmd) {
    return Response.ok(service.execCmd(cmd)).build();
  }

  @GET
  @RolesAllowed({"admin"})
  public Response getSessions() {

    List<ConnectionSession> sessions = service.getSessions();

    if (sessions.isEmpty()) {
      return Response.noContent().build();
    } else {
      return Response.ok(sessions).build();
    }
  }

  @DELETE
  @RolesAllowed({"admin"})
  public void closeAllSession() {
    service.deleteSessions();
  }

  /**
   * Close ssh session by envId.
   *
   * @param id of the environment instance
   * @return session responce
   */
  @DELETE
  @Path("/{envId}")
  @RolesAllowed({"user", "admin"})
  public Response closeSession(@PathParam("envId") UUID id) {

    ConnectionResponse response = service.closeSession(id);

    if (response.getStatus() == ConnSessionStatus.NOT_FOUND) {
      return Response.status(Status.NOT_FOUND).entity("Session with env id: " + id + " cannot be found.").build();
    } else {
      return Response.ok(response).build();
    }
  }

  public Response fallbackTimeoutRecommendations(CmdDTO cmd) {
    LOGGER.info("Falling back to RecommendationResource#fallbackTimeoutRecommendations()");
    // safe bet, return something that everybody likes
    return Response
        .status(Status.INTERNAL_SERVER_ERROR)
        .entity("Terminal session timeout for env. instance: " + cmd.getEnvId())
        .build();
  }
}

