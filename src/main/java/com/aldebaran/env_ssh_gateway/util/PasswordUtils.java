package com.aldebaran.env_ssh_gateway.util;

import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.security.spec.KeySpec;
import java.util.Base64;
import java.util.Random;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

public class PasswordUtils {

  private static final Random RANDOM = new SecureRandom();
  private static final String ALPHABET = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
  private static final int ITERATIONS = 65536;
  private static final int KEY_LENGTH = 256;
  private static final String PRIVATE_KEY = "Aldebaran_project_@_2020";

  private byte[] iv = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

  public static String getSalt(int length) {
    StringBuilder returnValue = new StringBuilder(length);
    for (int i = 0; i < length; i++) {
      returnValue.append(ALPHABET.charAt(RANDOM.nextInt(ALPHABET.length())));
    }
    return new String(returnValue);
  }

  public static SecretKeySpec getSecretKeySpec(byte[] salt) {

    try {
      SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
      KeySpec spec = new PBEKeySpec(PRIVATE_KEY.toCharArray(), salt, ITERATIONS, KEY_LENGTH);
      SecretKey tmp = factory.generateSecret(spec);

      return new SecretKeySpec(tmp.getEncoded(), "AES");
    } catch (Exception e) {
      System.out.println("Error while getting secret key spec: " + e.toString());
    }
    return null;
  }

  public static String generatePassword(String password, String salt) {
    byte[] iv = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    IvParameterSpec ivspec = new IvParameterSpec(iv);

    try {
      Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
      cipher.init(Cipher.ENCRYPT_MODE, getSecretKeySpec(salt.getBytes()), ivspec);
      return Base64.getEncoder().encodeToString(cipher.doFinal(password.getBytes(StandardCharsets.UTF_8)));
    } catch (Exception e) {
      System.out.println("Error while encrypting: " + e.toString());
    }
    return null;
  }

  public static String decodePassword(String password, String salt) {
    byte[] iv = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    IvParameterSpec ivspec = new IvParameterSpec(iv);

    try {
      Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
      cipher.init(Cipher.DECRYPT_MODE, getSecretKeySpec(salt.getBytes()), ivspec);
      return new String(cipher.doFinal(Base64.getDecoder().decode(password)));
    } catch (Exception e) {
      System.out.println("Error while decrypting: " + e.toString());
    }
    return null;
  }

  public static boolean verifyPassword(String providedPassword,
      String securedPassword, String salt) {

    try {
      // Generate New secure password with the same salt
      String newSecurePassword = generatePassword(providedPassword, salt);

      // Check if two passwords are equal
      return newSecurePassword.equalsIgnoreCase(securedPassword);
    } catch (Exception e) {
      System.out.println("Error while verifying passwords: " + e.toString());
    }

    return false;
  }
}