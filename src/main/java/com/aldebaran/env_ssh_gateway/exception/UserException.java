package com.aldebaran.env_ssh_gateway.exception;

/**
 * Represents all possible exceptions caused by user's actions.
 *
 * @author Tarasov Ivan
 * @since 0.0.1
 */
public class UserException extends Exception {

  public UserException() {
    super();
  }

  public UserException(String message) {
    super(message);
  }
}
