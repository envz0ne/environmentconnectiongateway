package com.aldebaran.env_ssh_gateway.exception;

public class RestConnectionException extends RuntimeException {

  public RestConnectionException() {
    super();
  }

  public RestConnectionException(String message) {
    super(message);
  }
}
