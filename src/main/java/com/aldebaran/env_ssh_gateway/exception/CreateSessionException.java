package com.aldebaran.env_ssh_gateway.exception;

public class CreateSessionException extends RuntimeException {

  public CreateSessionException() {
    super();
  }

  public CreateSessionException(String message) {
    super(message);
  }
}
