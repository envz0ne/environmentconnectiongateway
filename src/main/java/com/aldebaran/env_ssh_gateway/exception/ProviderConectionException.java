package com.aldebaran.env_ssh_gateway.exception;

public class ProviderConectionException extends Exception {

  public ProviderConectionException() {
    super();
  }

  public ProviderConectionException(String message) {
    super(message);
  }
}
