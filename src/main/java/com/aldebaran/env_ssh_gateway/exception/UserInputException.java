package com.aldebaran.env_ssh_gateway.exception;

/**
 * Represents user's exception caused by his input.
 *
 * @author Tarasov Ivan
 * @since 0.0.1
 */
public class UserInputException extends UserException {

}
