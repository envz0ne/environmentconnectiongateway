package com.aldebaran.env_ssh_gateway.exception;

/**
 * Represents user's exception caused by incorrect command.
 *
 * @author Tarasov Ivan
 * @since 0.0.1
 */
public class UserCmdException extends UserException {

}
