package com.aldebaran.env_ssh_gateway.service;

import com.aldebaran.env_ssh_gateway.exception.CreateSessionException;
import com.aldebaran.env_ssh_gateway.model.ConnSessionCreateConfig;
import com.aldebaran.env_ssh_gateway.model.ConnectionSession;

/**
 * Represents the session creator.
 *
 * @author Tarasov Ivan
 * @see 0.0.1
 */
public interface SessionCreator {

  /**
   * @param config has necessary params to create a new session
   * @return a new session
   */
  ConnectionSession createSession(ConnSessionCreateConfig config) throws CreateSessionException;
}
