package com.aldebaran.env_ssh_gateway.service;

import com.aldebaran.env_ssh_gateway.model.CmdDTO;
import com.aldebaran.env_ssh_gateway.model.ConnectionResponse;
import com.aldebaran.env_ssh_gateway.model.ConnectionSession;
import java.util.List;
import java.util.UUID;

/**
 * Represents the service for managing a ssh connection.
 *
 * @author Tarasov Ivan
 * @see 0.0.1
 */
public interface SessionService {

  /**
   * @param cmd is an object with command and id for specific running course.
   * @return session response
   */
  ConnectionResponse execCmd(CmdDTO cmd);

  /**
   * @param envId represents specific runId
   * @return session response
   */
  ConnectionResponse closeSession(UUID envId);

  List<ConnectionSession> getSessions();

  void deleteSessions();
}
