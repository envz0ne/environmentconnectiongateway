package com.aldebaran.env_ssh_gateway.service;

import com.aldebaran.env_ssh_gateway.model.CmdDTO;
import com.aldebaran.env_ssh_gateway.model.ConnectionResponse;
import com.aldebaran.env_ssh_gateway.model.ConnectionSession;

/**
 * Represents the ssh connection and execution util.
 *
 * @author Tarasov Ivan
 * @since 0.0.1
 */
public interface SessionConnectionHandler {

  /**
   * @param session that needs to be connected
   * @param cmd that needs to be executed over ssh connection
   * @return response of the accomplishment
   */
  ConnectionResponse execCmd(ConnectionSession session, CmdDTO cmd);
}
