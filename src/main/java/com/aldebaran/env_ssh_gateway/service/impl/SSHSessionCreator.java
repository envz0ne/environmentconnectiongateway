package com.aldebaran.env_ssh_gateway.service.impl;

import com.aldebaran.env_ssh_gateway.exception.CreateSessionException;
import com.aldebaran.env_ssh_gateway.handler.environment.ConnectionEnvironmentHandler;
import com.aldebaran.env_ssh_gateway.handler.environment.EnvironmentRunningStatus;
import com.aldebaran.env_ssh_gateway.model.ConnSessionCreateConfig;
import com.aldebaran.env_ssh_gateway.model.ConnectionSession;
import com.aldebaran.env_ssh_gateway.rest_client.EnvironmentProviderService;
import com.aldebaran.env_ssh_gateway.rest_client.model.Provider;
import com.aldebaran.env_ssh_gateway.rest_client.model.env.Environment;
import com.aldebaran.env_ssh_gateway.rest_client.model.env.EnvironmentStatus;
import com.aldebaran.env_ssh_gateway.service.SessionCreator;
import com.aldebaran.env_ssh_gateway.util.PasswordUtils;
import java.time.ZonedDateTime;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.eclipse.microprofile.rest.client.inject.RestClient;

/**
 * Represents the ssh session creator.
 *
 * @author Tarasov Ivan
 * @see 0.0.1
 */
@ApplicationScoped
public class SSHSessionCreator implements SessionCreator {

  @Inject
  @RestClient
  EnvironmentProviderService envProviderService;

  @Inject
  ConnectionEnvironmentHandler conEnvHandler;

  /**
   * Create a new ssh session.
   *
   * @param createConfig is object with parameters for creating ssh session
   * @return a new terminal session.
   */
  @Override
  public ConnectionSession createSession(ConnSessionCreateConfig createConfig) throws CreateSessionException {

    // Environment for which ssh session will be created
    Environment env;

    // Trying to get the environment by id
    try {

      Response response = envProviderService.getEnvironmentInstance(createConfig.getEnvId());

      if (response.getStatus() != Status.OK.getStatusCode()) {
        throw new CreateSessionException((String) response.getEntity());
      }

      env = response.readEntity(Environment.class);

    } catch (Exception exp) {
      throw new CreateSessionException("Environment Provider exception: " + exp.getMessage());
    }

    // Environment is running
    EnvironmentRunningStatus runningStatus = conEnvHandler.isEnvironmentRunning(createConfig.getEnvId());

    if (!runningStatus.isRunning() && runningStatus.getStatus() != EnvironmentStatus.PROVIDER_CLIENT_EXCEPTION) {
      throw new CreateSessionException("Environment with id: " + createConfig.getEnvId()
          + " is not running");
    }

    if (runningStatus.getStatus() == EnvironmentStatus.PROVIDER_CLIENT_EXCEPTION) {
      throw new CreateSessionException("Cannot connect to the provider's host");
    }

    Provider provider = env.getProvider();
    ConnectionSession connectionSession = new ConnectionSession();

    // getting provider data
    String user = provider.getLogin();
    String host = provider.getUrl().substring(6);

    // Set the new terminal session over ssh
    connectionSession.setHosts(env.getHosts());
    connectionSession.setEnvId(env.getId());
    connectionSession.setLogin(provider.getLogin());
    connectionSession.setPassword(
        PasswordUtils
            .decodePassword(provider.getPassword(), provider.getPasswordSalt()
            )
    );
    connectionSession.setPasswordSalt(provider.getPasswordSalt());
    connectionSession.setUrl(provider.getUrl());
    connectionSession.setCreated(ZonedDateTime.now());

    return connectionSession;
  }
}