package com.aldebaran.env_ssh_gateway.service.impl;

import com.aldebaran.env_ssh_gateway.exception.CreateSessionException;
import com.aldebaran.env_ssh_gateway.handler.session.SSHSessionConnectionHandler;
import com.aldebaran.env_ssh_gateway.model.CmdDTO;
import com.aldebaran.env_ssh_gateway.model.ConnSessionCreateConfig;
import com.aldebaran.env_ssh_gateway.model.ConnSessionStatus;
import com.aldebaran.env_ssh_gateway.model.ConnectionResponse;
import com.aldebaran.env_ssh_gateway.model.ConnectionSession;
import com.aldebaran.env_ssh_gateway.rest_client.EnvironmentProviderService;
import com.aldebaran.env_ssh_gateway.service.SessionService;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.jboss.logging.Logger;

/**
 * Represents the ssh service for managing a ssh connection.
 *
 * @author Tarasov Ivan
 * @see 0.0.1
 */
@ApplicationScoped
public class SSHSessionService implements SessionService {

  private static final Logger LOGGER = Logger.getLogger(SSHSessionService.class);
  /**
   * The map of sessions:
   *
   * key - envId value - TerminalSession
   */
  private Map<UUID, ConnectionSession> sessions = new ConcurrentHashMap<>();

  @Inject
  SSHSessionCreator sessionCreator;

  @Inject
  SSHSessionConnectionHandler sshConnectionHandler;

  @Inject
  @RestClient
  EnvironmentProviderService providerService;

  /**
   * Execute a cmd over ssh session
   *
   * @param cmd is an object with command and id for specific running course.
   * @return session response
   */
  @Override
  public ConnectionResponse execCmd(CmdDTO cmd) {

    // Ssh session connection response
    ConnectionResponse response = new ConnectionResponse();

    // Check cmd's properties
    // Create a new over ssh session connection
    if (!sessions.containsKey(cmd.getEnvId())) {

      ConnSessionCreateConfig config = new ConnSessionCreateConfig();
      config.setEnvId(cmd.getEnvId());

      LOGGER.warn("Session with envId: " + cmd.getEnvId() + " doesn't exsist.");

      try {
        // Adding a new session
        LOGGER.info("Try to create the new ssh session for envId: " + cmd.getEnvId());

        sessions.put(cmd.getEnvId(), sessionCreator.createSession(config));

        response.setStatus(ConnSessionStatus.OPENED);
        response.setMsg("Session with envId: " + cmd.getEnvId() + " was opened");

        LOGGER.info("Session with envId: " + cmd.getEnvId() + " was opened");

      } catch (CreateSessionException exp) {
        // Cannot create a ssh session for environment with id: envId
        LOGGER
            .error("Errors appear while creating the new ssh session for envId: " + cmd.getEnvId() + exp.getMessage());

        response.setStatus(ConnSessionStatus.NOT_OPENED);
        response.setMsg(exp.getMessage());
        return response;
      }
    }

    // If cmd is empty then ssh session needs to be opened
    if (!cmd.getCmd().isEmpty()) {
      response = sshConnectionHandler.execCmd(sessions.get(cmd.getEnvId()), cmd);
    } else {
      if (response.getStatus() == null) {
        response.setStatus(ConnSessionStatus.BAD_CMD);
        response.setMsg("Command is empty.");
      }
    }

    return response;
  }

  /**
   * Close ssh session for the environment instance by shutdown instance.
   *
   * @param envId represents specific runId
   * @return session response
   */
  @Override
  public ConnectionResponse closeSession(UUID envId) {

    ConnectionSession session = sessions.get(envId);

    ConnectionResponse response = new ConnectionResponse();

    if (session == null) {

      LOGGER.info("Session with envId: " + envId + " cannot be found.");

      response.setMsg("Session with envId: " + envId + " cannot be found.");
      response.setStatus(ConnSessionStatus.NOT_FOUND);
      return response;
    }

    return response;
  }

  @Override
  public List<ConnectionSession> getSessions() {
    return new ArrayList<>(sessions.values());
  }

  @Override
  public void deleteSessions() {
    sessions.clear();
  }
}