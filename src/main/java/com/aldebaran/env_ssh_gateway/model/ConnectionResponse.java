package com.aldebaran.env_ssh_gateway.model;

public class ConnectionResponse {

  private String msg;

  private ConnSessionStatus status;

  public String getMsg() {
    return msg;
  }

  public void setMsg(String msg) {
    this.msg = msg;
  }

  public ConnSessionStatus getStatus() {
    return status;
  }

  public void setStatus(ConnSessionStatus status) {
    this.status = status;
  }
}
