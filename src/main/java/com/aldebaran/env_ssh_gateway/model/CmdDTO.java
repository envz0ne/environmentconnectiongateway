package com.aldebaran.env_ssh_gateway.model;

import java.util.UUID;
import javax.validation.constraints.NotNull;

/**
 * Represents the command that will be entered into terminal.
 *
 * @author Tarasov Ivan
 * @see 0.0.1
 */
public class CmdDTO {

  @NotNull
  private UUID envId;

  private String cmd;

  public UUID getEnvId() {
    return envId;
  }

  public void setEnvId(UUID envId) {
    this.envId = envId;
  }

  public String getCmd() {
    return cmd;
  }

  public void setCmd(String cmd) {
    this.cmd = cmd;
  }
}
