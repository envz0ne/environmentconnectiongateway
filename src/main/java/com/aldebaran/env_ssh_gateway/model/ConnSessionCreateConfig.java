package com.aldebaran.env_ssh_gateway.model;

import java.util.UUID;
import javax.validation.constraints.NotBlank;

/**
 * Represents the session creation configuration.
 *
 * @author Tarasov Ivan
 * @see 0.0.1
 */
public class ConnSessionCreateConfig {

  @NotBlank
  private UUID envId;

  public UUID getEnvId() {
    return envId;
  }

  public void setEnvId(UUID envId) {
    this.envId = envId;
  }
}
