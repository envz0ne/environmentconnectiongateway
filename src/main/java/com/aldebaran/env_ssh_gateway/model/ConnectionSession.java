package com.aldebaran.env_ssh_gateway.model;

import com.aldebaran.env_ssh_gateway.rest_client.model.host.HostInstanceDTO;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.UUID;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * Represents the configuration of ssh connection to the specific environment.
 *
 * @author Tarasov Ivan
 * @since 0.0.1
 */
public class ConnectionSession {

  @NotNull
  private UUID envId;

  @NotNull
  private List<HostInstanceDTO> hosts;

  @NotBlank
  private String url;

  @NotBlank
  private String login;

  @NotBlank
  private String password;

  @NotBlank
  private String passwordSalt;

  private ZonedDateTime created;

  public UUID getEnvId() {
    return envId;
  }

  public void setEnvId(UUID envId) {
    this.envId = envId;
  }

  public List<HostInstanceDTO> getHosts() {
    return hosts;
  }

  public void setHosts(List<HostInstanceDTO> hosts) {
    this.hosts = hosts;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getLogin() {
    return login;
  }

  public void setLogin(String login) {
    this.login = login;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getPasswordSalt() {
    return passwordSalt;
  }

  public void setPasswordSalt(String passwordSalt) {
    this.passwordSalt = passwordSalt;
  }

  public ZonedDateTime getCreated() {
    return created;
  }

  public void setCreated(ZonedDateTime created) {
    this.created = created;
  }
}