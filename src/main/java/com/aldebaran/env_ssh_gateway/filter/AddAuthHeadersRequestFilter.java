package com.aldebaran.env_ssh_gateway.filter;

import java.io.IOException;
import java.util.List;
import java.util.Map.Entry;
import javax.inject.Inject;
import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.Provider;
import org.eclipse.microprofile.jwt.JsonWebToken;

@Provider
public class AddAuthHeadersRequestFilter implements ClientRequestFilter, ContainerRequestFilter {

  @Inject
  JsonWebToken jwt;

  private static ThreadLocal<MultivaluedMap<String, Object>> headersMap = new ThreadLocal<>();
  private final String[] headersToPropagate;

  public AddAuthHeadersRequestFilter() {
    headersToPropagate =
        System.getProperty("io.openliberty.propagate.headersToPropagate", "Authorization").split(",");
  }

  @Override
  public void filter(ContainerRequestContext reqContext) throws IOException {
    // invoked on incoming request to JAX-RS resource
    // save off the headers we are interested in into a thread local

    // Check exparation of the jwt;
    //if (Instant.ofEpochSecond(jwt.getExpirationTime()).compareTo(ZonedDateTime.now().toInstant()) <= 0) {
    // expiration time is out
    // need to refresh_token
    //}

    MultivaluedMap<String, String> headersFromRequest = reqContext.getHeaders();

    //System.out.println(jwt.getRawToken());

    if (headersFromRequest.get("Authorization") != null) {

      //System.out.println(headersFromRequest.get("Authorization"));

      MultivaluedMap<String, Object> headerMapToSend = new MultivaluedHashMap<>();
      for (String header : headersToPropagate) {
        for (String value : headersFromRequest.get(header)) {
          headerMapToSend.add(header, value);
        }
      }
      headersMap.remove();
      headersMap.set(headerMapToSend);
    }
  }

  @Override
  public void filter(ClientRequestContext reqContext) throws IOException {
    MultivaluedMap<String, Object> headersToSend = headersMap.get();
    if (headersToSend != null && !headersToSend.isEmpty()) {
      MultivaluedMap<String, Object> actualHeaders = reqContext.getHeaders();
      for (Entry<String, List<Object>> entry : headersToSend.entrySet()) {
        actualHeaders.addAll(entry.getKey(), entry.getValue());
      }
    }
    //headersMap.remove();
  }
}