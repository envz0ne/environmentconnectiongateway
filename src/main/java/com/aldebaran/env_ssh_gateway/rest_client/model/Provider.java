package com.aldebaran.env_ssh_gateway.rest_client.model;

import java.util.UUID;
import javax.validation.constraints.NotBlank;

/**
 * Represents a system that gives access to the environment.
 *
 * @author Tarasov Ivan
 * @since 0.0.1
 */
public class Provider {

  private UUID id;

  @NotBlank
  private String name;

  @NotBlank
  private String type;

  @NotBlank
  private String connectionType;

  @NotBlank
  private String url;

  @NotBlank
  private String login;

  @NotBlank
  private String password;

  @NotBlank
  private String passwordSalt;

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getConnectionType() {
    return connectionType;
  }

  public void setConnectionType(String connectionType) {
    this.connectionType = connectionType;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getLogin() {
    return login;
  }

  public void setLogin(String login) {
    this.login = login;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getPasswordSalt() {
    return passwordSalt;
  }

  public void setPasswordSalt(String passwordSalt) {
    this.passwordSalt = passwordSalt;
  }
}
