package com.aldebaran.env_ssh_gateway.rest_client.model.host;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * Represents host machine or container info.
 *
 * @author Tarasov Ivan
 * @since 0.0.1
 */
public class HostStatusInfo {

  @NotNull
  private String id;

  @NotNull
  private HostStatus status;

  @NotBlank
  private String msg;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public HostStatus getStatus() {
    return status;
  }

  public void setStatus(HostStatus status) {
    this.status = status;
  }

  public String getMsg() {
    return msg;
  }

  public void setMsg(String msg) {
    this.msg = msg;
  }
}
