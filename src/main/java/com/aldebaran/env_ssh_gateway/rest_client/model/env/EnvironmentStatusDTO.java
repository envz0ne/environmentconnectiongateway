package com.aldebaran.env_ssh_gateway.rest_client.model.env;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class EnvironmentStatusDTO {

  @NotNull
  private EnvironmentStatus status;

  @NotBlank
  private String msg;

  public EnvironmentStatus getStatus() {
    return status;
  }

  public void setStatus(EnvironmentStatus status) {
    this.status = status;
  }

  public String getMsg() {
    return msg;
  }

  public void setMsg(String msg) {
    this.msg = msg;
  }
}