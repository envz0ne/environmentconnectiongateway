package com.aldebaran.env_ssh_gateway.rest_client.model.host;

import java.util.UUID;
import javax.validation.constraints.NotNull;

public class HostInstanceDTO {

  @NotNull
  private UUID id;

  @NotNull
  private UUID hostId;

  @NotNull
  private String containerId;

  private String url;

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public UUID getHostId() {
    return hostId;
  }

  public void setHostId(UUID hostId) {
    this.hostId = hostId;
  }

  public String getContainerId() {
    return containerId;
  }

  public void setContainerId(String containerId) {
    this.containerId = containerId;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }
}
