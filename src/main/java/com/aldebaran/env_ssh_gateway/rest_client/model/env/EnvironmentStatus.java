package com.aldebaran.env_ssh_gateway.rest_client.model.env;

public enum EnvironmentStatus {
  ALREDY_CREATED,
  CREATED,
  RUNNING,
  NOT_STARTED,
  REMOVED,
  PAUSED,
  NOT_PAUSED,
  UNPAUSED,
  NOT_UNPAUSED,
  PENDING,
  STOPPED,
  NOT_STOPPED,
  UNKONWN,
  TEA_POT,
  NO_PROVIDER_CLIENT,
  PROVIDER_CLIENT_EXCEPTION
}