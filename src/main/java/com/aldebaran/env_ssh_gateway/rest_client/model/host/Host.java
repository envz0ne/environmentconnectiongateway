package com.aldebaran.env_ssh_gateway.rest_client.model.host;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.UUID;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * Represents host machine or container on which will be running something.
 *
 * @author Tarasov Ivan
 * @since 0.0.1
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Host {

  private UUID id;

/*
  @NotBlank
  private String name;
*/

  @NotBlank
  private String containerId;

  @NotNull
  private String url;

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public String getContainerId() {
    return containerId;
  }

  public void setContainerId(String containerId) {
    this.containerId = containerId;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

/*

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
*/
}
