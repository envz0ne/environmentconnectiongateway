package com.aldebaran.env_ssh_gateway.rest_client.model.env;

import java.util.UUID;
import javax.validation.constraints.NotNull;

public class EnvironmentResponseDTO {

  @NotNull
  private UUID id;

  @NotNull
  private EnvironmentStatusDTO info;

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public EnvironmentStatusDTO getInfo() {
    return info;
  }

  public void setInfo(EnvironmentStatusDTO info) {
    this.info = info;
  }
}