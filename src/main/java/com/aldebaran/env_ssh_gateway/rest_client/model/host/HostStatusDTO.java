package com.aldebaran.env_ssh_gateway.rest_client.model.host;

import com.aldebaran.env_ssh_gateway.rest_client.model.env.EnvironmentStatusDTO;
import java.util.List;
import java.util.UUID;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * Represents host machine or container dto.
 *
 * @author Tarasov Ivan
 * @since 0.0.1
 */
public class HostStatusDTO {

  @NotNull
  private UUID envId;

  @NotNull
  private List<HostStatusInfo> hosts;

  @NotBlank
  private EnvironmentStatusDTO envStatus;

  public UUID getEnvId() {
    return envId;
  }

  public void setEnvId(UUID envId) {
    this.envId = envId;
  }

  public List<HostStatusInfo> getHosts() {
    return hosts;
  }

  public void setHosts(List<HostStatusInfo> hosts) {
    this.hosts = hosts;
  }

  public EnvironmentStatusDTO getEnvStatus() {
    return envStatus;
  }

  public void setEnvStatus(EnvironmentStatusDTO envStatus) {
    this.envStatus = envStatus;
  }
}
