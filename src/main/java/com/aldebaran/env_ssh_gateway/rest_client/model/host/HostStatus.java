package com.aldebaran.env_ssh_gateway.rest_client.model.host;

/**
 * Represents host machine or container status.
 *
 * @author Tarasov Ivan
 * @since 0.0.1
 */
public enum HostStatus {
  RUNNING,
  NOT_STARTED,
  REMOVED,
  NOT_REMOVED,
  PAUSED,
  NOT_PAUSED,
  UNPAUSED,
  NOT_UNPAUSED,
  STOPPED,
  NOT_STOPPED,
  EXITED
}
