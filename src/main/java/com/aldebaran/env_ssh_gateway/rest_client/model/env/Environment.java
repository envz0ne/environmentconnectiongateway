package com.aldebaran.env_ssh_gateway.rest_client.model.env;

import com.aldebaran.env_ssh_gateway.rest_client.model.Provider;
import com.aldebaran.env_ssh_gateway.rest_client.model.host.HostInstanceDTO;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.List;
import java.util.UUID;
import javax.validation.constraints.NotNull;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Environment {

  @NotNull
  private UUID id;
  /**
   * Environment id.
   */
  @NotNull
  private UUID envId;

  @NotNull
  private Provider provider;

  private List<HostInstanceDTO> hosts;

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public UUID getEnvId() {
    return envId;
  }

  public void setEnvId(UUID envId) {
    this.envId = envId;
  }

  public Provider getProvider() {
    return provider;
  }

  public void setProvider(Provider provider) {
    this.provider = provider;
  }

  public List<HostInstanceDTO> getHosts() {
    return hosts;
  }

  public void setHosts(List<HostInstanceDTO> hosts) {
    this.hosts = hosts;
  }
}
