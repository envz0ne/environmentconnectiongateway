package com.aldebaran.env_ssh_gateway.rest_client;

import java.util.UUID;
import javax.enterprise.context.RequestScoped;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.jboss.resteasy.annotations.jaxrs.PathParam;

@RegisterRestClient(configKey = "env-provider-api")
@Path("/environment-provider/environments")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface EnvironmentProviderService {

  @GET
  @Path("/{id}/hosts/status")
  @RequestScoped
  Response getEnvHostsStatus(@PathParam("id") UUID id);

  @POST
  @Path("/{id}/restart")
  @RequestScoped
  Response restartEnvironment(@PathParam("id") UUID id);

  @Path("/{id}/terminate")
  @POST
  @RequestScoped
  Response stopEnvironment(@PathParam("id") UUID id);

  @Path("/instances/{id}")
  @GET
  @Transactional
  @RequestScoped
  Response getEnvironmentInstance(@PathParam("id") UUID id);
}