# EnvironmentSSHGateway

# HOW TO RUN WITH MAVEN

You can run this service by entering the next command:

mvn install clean package quarkus:dev

-Ddebug=5006
-DHTTP_PORT=5100
-DENV_PROVIDER_SERVICE=http://localhost:8180
-DSSH_CON_RETRY_LIMIT=5 -DSSH_CON_TIMELIFE=3